package ru.nsu.fit.borzov.server.model.variable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Embeddable
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
//@DiscriminatorValue(value = "D")
public class MyDouble{
    Double value;
}
