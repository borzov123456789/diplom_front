package legacy;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class ClientInfo {
    public String ip;
    public int port;
    public int size;
    public int rank;
}
