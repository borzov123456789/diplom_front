package parts;

import parts.lang.code.TaskExecutor;
import parts.lang.variable.Variable;
import parts.network.Server;
import parts.network.TaskNet;
import parts.view.AddTaskView;
import parts.view.Task;

import java.io.IOException;
import java.util.UUID;

public class IncredibleLib {
    Server net;
    int port;
    IncredibleLib(String ip, int port) {
        net = new Server(ip, port);
        this.port = port;
    }

    public void doElseWork() throws IOException {
        Task task = net.getTaskFromServer();
        while (task == null) {
            task = net.getTaskFromServer();
        }
        doWork(task);
    }

    public Variable[] doMyWork(AddTaskView addTaskView) throws IOException {
        UUID id = net.addTast(addTaskView);
        Task task = net.checkTaskStatus(id);
        while (task != null && !task.isStarted()) {
            task = net.checkTaskStatus(id);
        }
        Variable[] toReturn = doWork(task);
        net.deleteTask(task.getId());//fixme
        return toReturn;
    }

    private Variable[] doWork(Task task) throws IOException {
        TaskNet taskNet = new TaskNet(task, port);
        TaskExecutor taskExecutor = new TaskExecutor(task,taskNet);
        return taskExecutor.run();
    }
}
