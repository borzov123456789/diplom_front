package parts.network;

import lombok.Getter;
import parts.lang.variable.Matrix;
import parts.view.Client;
import parts.view.Task;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;

public class TaskNet {
    private int port;
    public Socket next;
    private ServerSocket serverSocket;
    public Socket prev;
    @Getter
    int size;
    @Getter
    int rank;

    Task task;

    public TaskNet(Task task, int port) {
        this.task = task;
        this.port = port;
        size = task.getUsersCount();
        rank = task.getCurrUserNumber();
        try {
            Client neighbour = task.getClients().get((rank + 1) % size);
            connectToNeighbour(neighbour);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void connectToNeighbour(Client client) throws IOException {
        serverSocket = new ServerSocket(port);
        while (true) {
            try {
                next = new Socket(client.getIp(), client.getPort());
                break;
            } catch (IOException e) {
                System.out.println("refused...");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
        prev = serverSocket.accept();
    }

    /*
    private void connectToNeighbours(String[] ips, int[] port) throws IOException {
        serverSocket = new ServerSocket(port[0]);
        while (true) {
            try {
                prev = new Socket(ips[0], port[1]);
                break;
            } catch (IOException e) {
                System.out.println("refused...");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
        next = serverSocket.accept();
    }
     */

    public void send(Matrix m, int part, int[] displs, int[] recvcounts) throws IOException {
        int rank = task.getCurrUserNumber();
        //int size = task.getUsersCount();
        //int h = m.width;
        double[] tmp = new double[m.data.length];//FIXME: в си можно будет сделать размер displs[part]
        //и поменять в MPI_Recv начальное значение счетчика цикла.
        if (rank % 2 == 0) {
            MPI_Ssend(m.width, m.data, displs[part], recvcounts[part], next);//(rank + 1) % size
            MPI_Recv(m.width, tmp, displs[part], recvcounts[part], prev); //(rank - 1 + size) % size
        } else {
            MPI_Recv(m.width, tmp, displs[part], recvcounts[part], prev); //(rank - 1 + size) % size
            MPI_Ssend(m.width, m.data, displs[part], recvcounts[part], next); //(rank + 1) % size
        }
        for (int i = displs[part]; i < recvcounts[part]; i++) {
            m.data[i] += tmp[i - displs[part]];
        }
    }

    public void MPI_Ssend(int w, double[] arr, int displ, int recvcount, Socket destination) throws IOException {
        //писать в destination
        byte[] toWrite = new byte[recvcount * w * Double.BYTES];
        for (int i = displ * w; i < (displ + recvcount) * w; i++) {
            byte[] b1 = toByteArray(arr[i]);
            System.arraycopy(b1, 0, toWrite, (i - displ * w) * Double.BYTES, Double.BYTES);
        }
        destination.getOutputStream().write(toWrite);
    }

    public void MPI_Recv(int w, double[] arr, int displ, int recvcount, Socket sender) throws IOException {
        //читать из sender
        byte[] toRead = new byte[recvcount * w * Double.BYTES];
        int size = 0;
        while (size != recvcount * w * Double.BYTES) {
            size += sender.getInputStream().read(toRead, size, toRead.length - size);
            //System.out.println("size: " + size);
        }
        //System.out.println("received");
        //int size = sender.getInputStream().read(toRead,0,toRead.length);
        if (size != recvcount * w * Double.BYTES) {
            try {
                throw new Exception("recived = " + size + " excepted " + recvcount * Double.BYTES);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //System.out.print("recv:");
        for (int i = 0; i < toRead.length / Double.BYTES; i++) {
            arr[i + displ * w] = toDouble(toRead, Double.BYTES * i);
            //System.out.print(arr[i + displ*w] + " ");
        }
        //System.out.println();
    }

    public void MPI_ALLGATHERV(int width, double[] arr, int[] displs, int[] recvcounts) throws IOException {
        int rank = task.getCurrUserNumber();
        int part = rank;
        //double[] tmp = new double[arr.length];
        for (int k = 0; k < size - 1; k++) {
            if (rank % 2 == 0) {
                MPI_Ssend(width, arr, displs[part], recvcounts[part], next);//(rank + 1) % size
                MPI_Recv(width, arr, displs[mod(part - 1, size)], recvcounts[mod(part - 1, size)], prev); //(rank - 1 + size) % size
            } else {
                MPI_Recv(width, arr, displs[mod(part - 1, size)], recvcounts[mod(part - 1, size)], prev); //(rank - 1 + size) % size
                MPI_Ssend(width, arr, displs[part], recvcounts[part], next); //(rank + 1) % size
            }
            part = (part - 1 + size) % size;
        }
    }

    private static byte[] toByteArray(double value) {
        byte[] bytes = new byte[8];
        ByteBuffer.wrap(bytes).putDouble(value);
        return bytes;
    }

    private static double toDouble(byte[] bytes, int startPos) {
        byte[] b = new byte[Double.BYTES];
        System.arraycopy(bytes, startPos, b, 0, b.length);
        return ByteBuffer.wrap(b).getDouble();
    }

    private void MPI_Send(byte[] value, Socket destination) throws IOException {
        destination.getOutputStream().write(value);
    }

    private void MPI_Recv(byte[] value, Socket sender) throws IOException {
        int size = 0;
        while (size != value.length) {
            size += sender.getInputStream().read(value, size, value.length - size);
            //System.out.println("size: " + size);
        }
        //System.out.println("received");
        //int size = sender.getInputStream().read(toRead,0,toRead.length);
        if (size != value.length) {
            try {
                throw new Exception("recived = " + size + " excepted " + value.length);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public double AllReduce(double value) throws IOException {
        byte[] tmp = new byte[Double.BYTES];
        double[] results = new double[size];
        double summ = 0;
        for (int i = 0; i < size; i++) {
            if (rank % 2 == 0) {
                MPI_Send(toByteArray(value), next);
                MPI_Recv(tmp, prev); //(rank - 1 + size) % size
            } else {
                MPI_Recv(tmp, prev); //(rank - 1 + size) % size
                MPI_Send(toByteArray(value), next);
            }
            value = toDouble(tmp, 0);
            results[(i + rank) % size] = toDouble(tmp, 0);
        }
        for (int i = 0; i < results.length; i++) {
            summ += results[i];
        }
        return summ;
    }

    private int mod(int v, int p) {
        return (v % p + p) % p;
    }
}
