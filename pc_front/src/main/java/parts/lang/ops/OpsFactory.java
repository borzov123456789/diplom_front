package parts.lang.ops;

import parts.network.TaskNet;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OpsFactory {
    static List<Class<? extends Operation>> opsClasses;

    private static void initFactory() {
        if (opsClasses == null) {
            opsClasses = new ArrayList<>();
            opsClasses.add(Summ.class);
            opsClasses.add(Mul.class);
            opsClasses.add(ScalarProduct.class);
            opsClasses.add(Sqrt.class);
            opsClasses.add(MulD.class);
            opsClasses.add(Sub.class);
            opsClasses.add(DivDD.class);
            //fixme: написать алгоритм, чтобы все наследники Operation сами добавлялись в список.
        }
    }

    public static Operation[] createOps(TaskNet taskNet) {
        initFactory();
        Operation[] ops = new Operation[opsClasses.size()];
        try {
            for (int i = 0; i < ops.length; i++) {
                ops[i] = opsClasses.get(i).getConstructor(TaskNet.class).newInstance(taskNet);
                Method m = opsClasses.get(i).getMethod("setId", Integer.class);
                m.invoke(ops[i], i);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        return ops;
    }

    public static Map<String, Integer> getNameToIdMap() {
        initFactory();
        //FIXME: для отладки так, в итоге этот метод будет работать по-другому
        Map<String, Integer> ops = new HashMap<>();
        ops.put("summ",0);
        ops.put("mul",1);
        ops.put("scalar",2);
        ops.put("sqrt",3);
        ops.put("muld",4);
        ops.put("sub",5);
        ops.put("divdd",6);
        /*
        try {
            for (int i = 0; i < opsClasses.size(); i++) {
                ops.put((String)opsClasses.get(i).getMethod("getName",null).invoke(null,null),i);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        */
        return ops;
    }
}
