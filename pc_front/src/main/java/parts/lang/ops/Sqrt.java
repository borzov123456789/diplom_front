package parts.lang.ops;

import parts.lang.variable.MyDouble;
import parts.lang.variable.Variable;
import parts.network.TaskNet;

public class Sqrt implements Operation {
    TaskNet taskNet;

    private int id;

    public Sqrt(TaskNet taskNet) {
        this.taskNet = taskNet;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void run(Variable[] args) {
        MyDouble result = args[0].getDoubleObj();
        double input = Math.sqrt(args[1].getDoubleObj().getValue());
        result.setValue(input);
    }
}
