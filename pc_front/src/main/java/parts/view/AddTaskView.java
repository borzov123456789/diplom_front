package parts.view;

import lombok.Data;
import parts.lang.code.Row;
import parts.lang.variable.Variable;

import java.util.*;

@Data
public class AddTaskView {
    int usersCount;
    List<Variable> context = new ArrayList<>();
    List<Row> code = new ArrayList<>();
}
