package parts.view;

import lombok.Data;
import parts.lang.code.Row;
import parts.lang.variable.Variable;

import java.util.*;

@Data
public class Task {
    UUID id;
    int usersCount;
    int currUserNumber;
    List<Client> clients = new ArrayList<>();
    boolean isStarted = false;
    Variable[] context;
    Row[] code;
}
